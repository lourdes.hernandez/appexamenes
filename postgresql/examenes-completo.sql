--
-- PostgreSQL database dump
--

-- Dumped from database version 14.4 (Debian 14.4-1.pgdg110+1)
-- Dumped by pg_dump version 14.4

-- Started on 2022-07-05 20:19:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16684)
-- Name: alumno; Type: TABLE; Schema: public; Owner: root
--
CREATE DATABASE examenes;

CREATE TABLE public.alumno (
    id_alumno integer DEFAULT nextval(('"alumno_id_alumno_seq"'::text)::regclass) NOT NULL,
    nombres character varying(100) NOT NULL,
    apellidos character varying(100) NOT NULL,
    id_experiencia_educativa integer,
    id_usuario integer NOT NULL
);


ALTER TABLE public.alumno OWNER TO root;

--
-- TOC entry 210 (class 1259 OID 16688)
-- Name: alumno_id_alumno_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.alumno_id_alumno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.alumno_id_alumno_seq OWNER TO root;

--
-- TOC entry 211 (class 1259 OID 16689)
-- Name: examen; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.examen (
    id_examen integer DEFAULT nextval(('"examen_id_examen_seq"'::text)::regclass) NOT NULL,
    nombre character varying(100) NOT NULL,
    id_experiencia_educativa integer NOT NULL,
    momento_apertura timestamp without time zone,
    momento_cierre timestamp without time zone,
    cerrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.examen OWNER TO root;

--
-- TOC entry 212 (class 1259 OID 16694)
-- Name: examen_alumno; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.examen_alumno (
    id_examen_alumno integer DEFAULT nextval(('"examen_alumno_id_examen_alumno_seq"'::text)::regclass) NOT NULL,
    id_alumno integer NOT NULL,
    id_examen integer NOT NULL,
    momento_inicio timestamp without time zone,
    momento_fin timestamp without time zone,
    calificacion numeric(4,2)
);


ALTER TABLE public.examen_alumno OWNER TO root;

--
-- TOC entry 213 (class 1259 OID 16698)
-- Name: examen_alumno_id_examen_alumno_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.examen_alumno_id_examen_alumno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.examen_alumno_id_examen_alumno_seq OWNER TO root;

--
-- TOC entry 214 (class 1259 OID 16699)
-- Name: examen_id_examen_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.examen_id_examen_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.examen_id_examen_seq OWNER TO root;

--
-- TOC entry 215 (class 1259 OID 16700)
-- Name: examen_reactivo; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.examen_reactivo (
    id_examen_reactivo integer DEFAULT nextval(('"examen_reactivo_id_examen_reactivo_seq"'::text)::regclass) NOT NULL,
    id_examen integer NOT NULL,
    id_reactivo integer NOT NULL,
    posicion integer
);


ALTER TABLE public.examen_reactivo OWNER TO root;

--
-- TOC entry 216 (class 1259 OID 16704)
-- Name: examen_reactivo_id_examen_reactivo_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.examen_reactivo_id_examen_reactivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.examen_reactivo_id_examen_reactivo_seq OWNER TO root;

--
-- TOC entry 217 (class 1259 OID 16705)
-- Name: experiencia_educativa; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.experiencia_educativa (
    id_experiencia_educativa integer DEFAULT nextval(('"experiencia_educativa_id_experiencia_educativa_seq"'::text)::regclass) NOT NULL,
    nombre character varying(200) NOT NULL,
    nrc integer NOT NULL,
    id_maestro integer
);


ALTER TABLE public.experiencia_educativa OWNER TO root;

--
-- TOC entry 218 (class 1259 OID 16709)
-- Name: experiencia_educativa_id_experiencia_educativa_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.experiencia_educativa_id_experiencia_educativa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.experiencia_educativa_id_experiencia_educativa_seq OWNER TO root;

--
-- TOC entry 219 (class 1259 OID 16710)
-- Name: maestro; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.maestro (
    id_maestro integer DEFAULT nextval(('"maestro_id_maestro_seq"'::text)::regclass) NOT NULL,
    nombre_completo character varying(100) NOT NULL,
    id_usuario integer NOT NULL
);


ALTER TABLE public.maestro OWNER TO root;

--
-- TOC entry 220 (class 1259 OID 16714)
-- Name: maestro_id_maestro_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.maestro_id_maestro_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.maestro_id_maestro_seq OWNER TO root;

--
-- TOC entry 221 (class 1259 OID 16715)
-- Name: reactivo; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.reactivo (
    id_reactivo integer DEFAULT nextval(('"reactivo_id_reactivo_seq"'::text)::regclass) NOT NULL,
    id_experiencia_educativa integer,
    texto character varying(200) NOT NULL,
    opcion_1 character varying(100) NOT NULL,
    opcion_2 character varying(100) NOT NULL,
    opcion_3 character varying(100),
    opcion_4 character varying(100),
    opcion_correcta integer NOT NULL
);


ALTER TABLE public.reactivo OWNER TO root;

--
-- TOC entry 222 (class 1259 OID 16721)
-- Name: reactivo_id_reactivo_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.reactivo_id_reactivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reactivo_id_reactivo_seq OWNER TO root;

--
-- TOC entry 223 (class 1259 OID 16722)
-- Name: respuesta_reactivo; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.respuesta_reactivo (
    id_respuesta_reactivo integer DEFAULT nextval(('"respuesta_reactivo_id_respuesta_reactivo_seq"'::text)::regclass) NOT NULL,
    id_examen_alumno integer DEFAULT nextval(('"respuesta_reactivo_id_examen_alumno_seq"'::text)::regclass) NOT NULL,
    opcion integer NOT NULL,
    correcta boolean,
    id_reactivo integer
);


ALTER TABLE public.respuesta_reactivo OWNER TO root;

--
-- TOC entry 224 (class 1259 OID 16727)
-- Name: respuesta_reactivo_id_examen_alumno_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.respuesta_reactivo_id_examen_alumno_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.respuesta_reactivo_id_examen_alumno_seq OWNER TO root;

--
-- TOC entry 225 (class 1259 OID 16728)
-- Name: respuesta_reactivo_id_respuesta_reactivo_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.respuesta_reactivo_id_respuesta_reactivo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.respuesta_reactivo_id_respuesta_reactivo_seq OWNER TO root;

--
-- TOC entry 226 (class 1259 OID 16729)
-- Name: rol; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.rol (
    id_rol integer DEFAULT nextval(('"rol_id_rol_seq"'::text)::regclass) NOT NULL,
    nombre character varying(50) NOT NULL
);


ALTER TABLE public.rol OWNER TO root;

--
-- TOC entry 227 (class 1259 OID 16733)
-- Name: rol_id_rol_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.rol_id_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.rol_id_rol_seq OWNER TO root;

--
-- TOC entry 228 (class 1259 OID 16734)
-- Name: usuario; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.usuario (
    id_usuario integer DEFAULT nextval(('"usuario_id_usuario_seq"'::text)::regclass) NOT NULL,
    login character varying NOT NULL,
    password character varying(255),
    id_rol integer NOT NULL,
    activo boolean NOT NULL
);


ALTER TABLE public.usuario OWNER TO root;

--
-- TOC entry 229 (class 1259 OID 16740)
-- Name: usuario_id_usuario_seq; Type: SEQUENCE; Schema: public; Owner: root
--

CREATE SEQUENCE public.usuario_id_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_usuario_seq OWNER TO root;

--
-- TOC entry 3409 (class 0 OID 16684)
-- Dependencies: 209
-- Data for Name: alumno; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.alumno (id_alumno, nombres, apellidos, id_experiencia_educativa, id_usuario) FROM stdin;
1	Ivanka	Morales	1	2
2	Luis	Perez	1	3
\.


--
-- TOC entry 3411 (class 0 OID 16689)
-- Dependencies: 211
-- Data for Name: examen; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.examen (id_examen, nombre, id_experiencia_educativa, momento_apertura, momento_cierre, cerrado) FROM stdin;
1	Parcial 1	1	2022-06-01 05:00:00	2022-06-01 07:00:00	t
2	Parcial 2	1	2022-06-15 05:00:00	2022-06-15 08:00:00	t
3	Ordinario	1	2022-07-10 05:00:00	2022-07-10 08:00:00	f
5	Titulo	1	2022-07-18 05:00:00	2022-07-18 09:00:00	f
\.


--
-- TOC entry 3412 (class 0 OID 16694)
-- Dependencies: 212
-- Data for Name: examen_alumno; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.examen_alumno (id_examen_alumno, id_alumno, id_examen, momento_inicio, momento_fin, calificacion) FROM stdin;
7	1	1	2022-07-05 14:18:01.926	2022-07-05 14:43:23.765	6.67
8	1	3	2022-07-05 17:00:38.864	2022-07-05 17:58:55.752	0.00
9	2	3	2022-07-05 18:03:37.226	2022-07-05 18:03:42.304	10.00
10	2	5	2022-07-05 19:44:03.855	\N	\N
\.


--
-- TOC entry 3415 (class 0 OID 16700)
-- Dependencies: 215
-- Data for Name: examen_reactivo; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.examen_reactivo (id_examen_reactivo, id_examen, id_reactivo, posicion) FROM stdin;
1	1	1	\N
2	1	2	\N
3	2	3	\N
4	2	1	\N
12	3	3	\N
13	1	3	\N
14	5	1	\N
15	5	2	\N
16	5	3	\N
17	5	5	\N
\.


--
-- TOC entry 3417 (class 0 OID 16705)
-- Dependencies: 217
-- Data for Name: experiencia_educativa; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.experiencia_educativa (id_experiencia_educativa, nombre, nrc, id_maestro) FROM stdin;
1	Desarrollo de aplicaciones	12345	1
\.


--
-- TOC entry 3419 (class 0 OID 16710)
-- Dependencies: 219
-- Data for Name: maestro; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.maestro (id_maestro, nombre_completo, id_usuario) FROM stdin;
1	Lourdes Hdez	1
\.


--
-- TOC entry 3421 (class 0 OID 16715)
-- Dependencies: 221
-- Data for Name: reactivo; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.reactivo (id_reactivo, id_experiencia_educativa, texto, opcion_1, opcion_2, opcion_3, opcion_4, opcion_correcta) FROM stdin;
3	1	Es un framework para clientes web	NodeJs	JUnit	Vue	Pandas	3
1	1	Qué es un requisito de software	Una característica	Una restricción	Una tarea	\N	1
2	1	Es una prueba de varios componentes	Unitaria	De caja blanca	De integración		3
5	1	Patrón arquitectónico común en clientes web	FDA	MVVM	SOA	Singleton	2
\.


--
-- TOC entry 3423 (class 0 OID 16722)
-- Dependencies: 223
-- Data for Name: respuesta_reactivo; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.respuesta_reactivo (id_respuesta_reactivo, id_examen_alumno, opcion, correcta, id_reactivo) FROM stdin;
\.


--
-- TOC entry 3426 (class 0 OID 16729)
-- Dependencies: 226
-- Data for Name: rol; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.rol (id_rol, nombre) FROM stdin;
1	Maestro
2	Alumno
\.


--
-- TOC entry 3428 (class 0 OID 16734)
-- Dependencies: 228
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.usuario (id_usuario, login, password, id_rol, activo) FROM stdin;
1	Lulu	123	1	t
2	imorales	12345	2	t
3	lperez	12345	2	t
\.


--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 210
-- Name: alumno_id_alumno_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.alumno_id_alumno_seq', 2, true);


--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 213
-- Name: examen_alumno_id_examen_alumno_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.examen_alumno_id_examen_alumno_seq', 10, true);


--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 214
-- Name: examen_id_examen_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.examen_id_examen_seq', 6, true);


--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 216
-- Name: examen_reactivo_id_examen_reactivo_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.examen_reactivo_id_examen_reactivo_seq', 17, true);


--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 218
-- Name: experiencia_educativa_id_experiencia_educativa_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.experiencia_educativa_id_experiencia_educativa_seq', 1, true);


--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 220
-- Name: maestro_id_maestro_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.maestro_id_maestro_seq', 1, true);


--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 222
-- Name: reactivo_id_reactivo_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.reactivo_id_reactivo_seq', 5, true);


--
-- TOC entry 3442 (class 0 OID 0)
-- Dependencies: 224
-- Name: respuesta_reactivo_id_examen_alumno_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.respuesta_reactivo_id_examen_alumno_seq', 1, false);


--
-- TOC entry 3443 (class 0 OID 0)
-- Dependencies: 225
-- Name: respuesta_reactivo_id_respuesta_reactivo_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.respuesta_reactivo_id_respuesta_reactivo_seq', 1, false);


--
-- TOC entry 3444 (class 0 OID 0)
-- Dependencies: 227
-- Name: rol_id_rol_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.rol_id_rol_seq', 2, true);


--
-- TOC entry 3445 (class 0 OID 0)
-- Dependencies: 229
-- Name: usuario_id_usuario_seq; Type: SEQUENCE SET; Schema: public; Owner: root
--

SELECT pg_catalog.setval('public.usuario_id_usuario_seq', 3, true);


--
-- TOC entry 3228 (class 2606 OID 16742)
-- Name: alumno PK_alumno; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT "PK_alumno" PRIMARY KEY (id_alumno);


--
-- TOC entry 3231 (class 2606 OID 16744)
-- Name: examen PK_examen; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen
    ADD CONSTRAINT "PK_examen" PRIMARY KEY (id_examen);


--
-- TOC entry 3235 (class 2606 OID 16746)
-- Name: examen_alumno PK_examen_alumno; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen_alumno
    ADD CONSTRAINT "PK_examen_alumno" PRIMARY KEY (id_examen_alumno);


--
-- TOC entry 3239 (class 2606 OID 16748)
-- Name: examen_reactivo PK_examen_reactivo; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen_reactivo
    ADD CONSTRAINT "PK_examen_reactivo" PRIMARY KEY (id_examen_reactivo);


--
-- TOC entry 3242 (class 2606 OID 16750)
-- Name: experiencia_educativa PK_experiencia_educativa; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.experiencia_educativa
    ADD CONSTRAINT "PK_experiencia_educativa" PRIMARY KEY (id_experiencia_educativa);


--
-- TOC entry 3245 (class 2606 OID 16752)
-- Name: maestro PK_maestro; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.maestro
    ADD CONSTRAINT "PK_maestro" PRIMARY KEY (id_maestro);


--
-- TOC entry 3248 (class 2606 OID 16754)
-- Name: reactivo PK_reactivo; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.reactivo
    ADD CONSTRAINT "PK_reactivo" PRIMARY KEY (id_reactivo);


--
-- TOC entry 3253 (class 2606 OID 16756)
-- Name: respuesta_reactivo PK_respuesta_reactivo; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.respuesta_reactivo
    ADD CONSTRAINT "PK_respuesta_reactivo" PRIMARY KEY (id_respuesta_reactivo);


--
-- TOC entry 3255 (class 2606 OID 16758)
-- Name: rol PK_rol; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.rol
    ADD CONSTRAINT "PK_rol" PRIMARY KEY (id_rol);


--
-- TOC entry 3257 (class 2606 OID 16760)
-- Name: usuario PK_usuario; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT "PK_usuario" PRIMARY KEY (id_usuario);


--
-- TOC entry 3225 (class 1259 OID 16761)
-- Name: IXFK_alumno_experiencia_educativa; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_alumno_experiencia_educativa" ON public.alumno USING btree (id_experiencia_educativa);


--
-- TOC entry 3226 (class 1259 OID 16762)
-- Name: IXFK_alumno_usuario; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_alumno_usuario" ON public.alumno USING btree (id_usuario);


--
-- TOC entry 3232 (class 1259 OID 16763)
-- Name: IXFK_examen_alumno_alumno; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_examen_alumno_alumno" ON public.examen_alumno USING btree (id_alumno);


--
-- TOC entry 3233 (class 1259 OID 16764)
-- Name: IXFK_examen_alumno_examen; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_examen_alumno_examen" ON public.examen_alumno USING btree (id_examen);


--
-- TOC entry 3229 (class 1259 OID 16765)
-- Name: IXFK_examen_experiencia_educativa; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_examen_experiencia_educativa" ON public.examen USING btree (id_experiencia_educativa);


--
-- TOC entry 3236 (class 1259 OID 16766)
-- Name: IXFK_examen_reactivo_examen; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_examen_reactivo_examen" ON public.examen_reactivo USING btree (id_examen);


--
-- TOC entry 3237 (class 1259 OID 16767)
-- Name: IXFK_examen_reactivo_reactivo; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_examen_reactivo_reactivo" ON public.examen_reactivo USING btree (id_reactivo);


--
-- TOC entry 3240 (class 1259 OID 16768)
-- Name: IXFK_experiencia_educativa_maestro; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_experiencia_educativa_maestro" ON public.experiencia_educativa USING btree (id_maestro);


--
-- TOC entry 3243 (class 1259 OID 16769)
-- Name: IXFK_maestro_usuario; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_maestro_usuario" ON public.maestro USING btree (id_usuario);


--
-- TOC entry 3246 (class 1259 OID 16770)
-- Name: IXFK_reactivo_experiencia_educativa; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_reactivo_experiencia_educativa" ON public.reactivo USING btree (id_experiencia_educativa);


--
-- TOC entry 3249 (class 1259 OID 16771)
-- Name: IXFK_respuesta_reactivo_examen_alumno; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_respuesta_reactivo_examen_alumno" ON public.respuesta_reactivo USING btree (id_examen_alumno);


--
-- TOC entry 3250 (class 1259 OID 16772)
-- Name: IXFK_respuesta_reactivo_examen_reactivo; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_respuesta_reactivo_examen_reactivo" ON public.respuesta_reactivo USING btree (id_reactivo);


--
-- TOC entry 3251 (class 1259 OID 16773)
-- Name: IXFK_respuesta_reactivo_reactivo; Type: INDEX; Schema: public; Owner: root
--

CREATE INDEX "IXFK_respuesta_reactivo_reactivo" ON public.respuesta_reactivo USING btree (id_reactivo);


--
-- TOC entry 3258 (class 2606 OID 16774)
-- Name: alumno FK_alumno_experiencia_educativa; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT "FK_alumno_experiencia_educativa" FOREIGN KEY (id_experiencia_educativa) REFERENCES public.experiencia_educativa(id_experiencia_educativa);


--
-- TOC entry 3259 (class 2606 OID 16779)
-- Name: alumno FK_alumno_usuario; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.alumno
    ADD CONSTRAINT "FK_alumno_usuario" FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);


--
-- TOC entry 3261 (class 2606 OID 16784)
-- Name: examen_alumno FK_examen_alumno_alumno; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen_alumno
    ADD CONSTRAINT "FK_examen_alumno_alumno" FOREIGN KEY (id_alumno) REFERENCES public.alumno(id_alumno);


--
-- TOC entry 3262 (class 2606 OID 16789)
-- Name: examen_alumno FK_examen_alumno_examen; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen_alumno
    ADD CONSTRAINT "FK_examen_alumno_examen" FOREIGN KEY (id_examen) REFERENCES public.examen(id_examen);


--
-- TOC entry 3260 (class 2606 OID 16794)
-- Name: examen FK_examen_experiencia_educativa; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen
    ADD CONSTRAINT "FK_examen_experiencia_educativa" FOREIGN KEY (id_experiencia_educativa) REFERENCES public.experiencia_educativa(id_experiencia_educativa);


--
-- TOC entry 3263 (class 2606 OID 16799)
-- Name: examen_reactivo FK_examen_reactivo_examen; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen_reactivo
    ADD CONSTRAINT "FK_examen_reactivo_examen" FOREIGN KEY (id_examen) REFERENCES public.examen(id_examen);


--
-- TOC entry 3264 (class 2606 OID 16804)
-- Name: examen_reactivo FK_examen_reactivo_reactivo; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.examen_reactivo
    ADD CONSTRAINT "FK_examen_reactivo_reactivo" FOREIGN KEY (id_reactivo) REFERENCES public.reactivo(id_reactivo);


--
-- TOC entry 3265 (class 2606 OID 16809)
-- Name: experiencia_educativa FK_experiencia_educativa_maestro; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.experiencia_educativa
    ADD CONSTRAINT "FK_experiencia_educativa_maestro" FOREIGN KEY (id_maestro) REFERENCES public.maestro(id_maestro);


--
-- TOC entry 3266 (class 2606 OID 16814)
-- Name: maestro FK_maestro_usuario; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.maestro
    ADD CONSTRAINT "FK_maestro_usuario" FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);


--
-- TOC entry 3267 (class 2606 OID 16819)
-- Name: reactivo FK_reactivo_experiencia_educativa; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.reactivo
    ADD CONSTRAINT "FK_reactivo_experiencia_educativa" FOREIGN KEY (id_experiencia_educativa) REFERENCES public.experiencia_educativa(id_experiencia_educativa);


--
-- TOC entry 3268 (class 2606 OID 16824)
-- Name: respuesta_reactivo FK_respuesta_reactivo_examen_alumno; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.respuesta_reactivo
    ADD CONSTRAINT "FK_respuesta_reactivo_examen_alumno" FOREIGN KEY (id_examen_alumno) REFERENCES public.examen_alumno(id_examen_alumno);


--
-- TOC entry 3269 (class 2606 OID 16829)
-- Name: respuesta_reactivo FK_respuesta_reactivo_reactivo; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.respuesta_reactivo
    ADD CONSTRAINT "FK_respuesta_reactivo_reactivo" FOREIGN KEY (id_reactivo) REFERENCES public.reactivo(id_reactivo);


-- Completed on 2022-07-05 20:19:08

--
-- PostgreSQL database dump complete
--

