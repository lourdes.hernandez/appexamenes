# Ejecucion en contenedor: Depende de Docker y Java

# Si se quiere generar frontend desde codigo, se require Node y npm 8.6.+
# cd frontend; npm install; npm build; cp -Rv dist/spa/* ..src/main/resources/static/

# Genera ejecutable de proyecto Spring
# (Ya incluye frontend)
./mvnw package

# Genera nueva imagen de Docker
docker build -t appexamenes:1.0 .

# Genera imagen para base de datos preparada
cd postgresql; docker build -t appexamenes_pg:1.0 .

# Corre imagenes generadas con Docker
cd ..
docker compose up

