package mx.uv.appexamenes.servicios;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;
import mx.uv.appexamenes.entidades.Examen;
import mx.uv.appexamenes.entidades.ExperienciaEducativa;
import mx.uv.appexamenes.repositorios.RepositorioExamenes;
import mx.uv.appexamenes.servicios.impl.ServicioExamenesImpl;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;
import static org.mockito.AdditionalAnswers.*;

/**
 *
 * @author MariadeLourdesHernan
 */
@ExtendWith(MockitoExtension.class)
public class ServicioExamenesTest {
    
    static ExperienciaEducativa ee;
    
    @Mock
    RepositorioExamenes repoExamenes;
    
    @InjectMocks
    ServicioExamenesImpl servExamenes;
    
    @BeforeAll
    static void prepararObjetosAuxiliares() {
        ee = new ExperienciaEducativa(1, "Sistemas en Red", 23456);
    }
    
    @Test
    void estadoExamenNuevo() {
        Examen ex = new Examen();
        ex.setNombre("Final");
        ex.setMomentoApertura(Date.from(Instant.parse("2022-07-20T09:00:00Z")));
        ex.setMomentoCierre(Date.from(Instant.parse("2022-07-20T12:00:00Z")));
        ex.setIdExperienciaEducativa(ee);
        when(repoExamenes.save(any(Examen.class))).then(returnsFirstArg());
        Examen resultado = servExamenes.save(ex);
        assertThat(resultado.getCerrado()).isFalse();
        
    }
    
    @Test()
    void examenFechasIncorrectas() {
        Examen ex = new Examen();
        ex.setNombre("Parcial");
        ex.setMomentoApertura(Date.from(Instant.parse("2022-07-20T09:00:00Z")));
        ex.setMomentoCierre(Date.from(Instant.parse("2022-07-20T08:00:00Z")));
        ex.setIdExperienciaEducativa(ee);
        //when(repoExamenes.save(any(Examen.class))).then(returnsFirstArg());
        Assertions.assertThrows(IllegalArgumentException.class ,() -> {servExamenes.save(ex);});
        
    }
}
