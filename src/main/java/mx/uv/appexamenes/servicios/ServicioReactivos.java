package mx.uv.appexamenes.servicios;

import mx.uv.appexamenes.entidades.Reactivo;

/**
 *
 * @author MariadeLourdesHernan
 */
public interface ServicioReactivos {
    
    public Reactivo guardar(Reactivo reactivo);
    
    public Reactivo actualizar(Reactivo reactivo);
    
    public void borrar(Integer idReactivo);

}
